(use-modules (skribilo engine)       ;; provides `find-engine'
             (skribilo evaluator)    ;; provides `evaluate-document'
             (skribilo package base) ;; provides `chapter', etc.
             (srfi srfi-1))

(let (;; Select an engine, i.e., an output format.
      (e (find-engine 'html))

      ;; Create a document.
      (d (load "index.skb")))

  ;; "Evaluate" the document to an HTML file.
  (with-output-to-file "index.html"
    (lambda ()
      (evaluate-document d e))))
